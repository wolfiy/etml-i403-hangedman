﻿/// ETML
/// Auteur: Sébastien TILLE
/// Date: 2 novembre 2023
/// Description: jeu du pendu en console réalisé pour le test pratique de C#
/// 
/// Remarque: un bug se glisse en suivant le structogramme, si la même lettre est
/// entrée deux fois par l'utilisateur, le compteur de bonne lettres est incrémenté 
/// deux fois et donc la condition de victoire est altérée. Considérer que la même
/// lettre ne sera pas entrée deux fois.
/// 
/// Une possible solution serait d'ajouter une variable string guess = ""; à la ligne
/// 85, d'ajouter à la ligne 144 le code suivant:
/// for (int i = 0; i < foundLetters.Length; i++)
/// {
///     guess += foundLetters[i];
/// }
/// et de changer la condition de la ligne 146 par
/// word == guess
/// Cela n'empêche pas d'entrer deux fois la même lettre, mais au moins les conditions
/// de victoires resteraient valides. La variable guess devrait être réinitialisée à 
/// chaque tentatives.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace etml_i403_hangedman
{
    /// <summary>
    /// Jeu du pendu.
    /// </summary>
    internal class Program
    {
        /// <summary>
        /// Méthode principale du programme.
        /// </summary>
        /// <param name="args">arguments d'exécution</param>
        static void Main(string[] args)
        {
            // Le nombre d'essai maximal
            const int NB_STEP = 8;
            
            // Le nombre de lignes nécessaire à dessiner le pendu
            const int NB_LINE = 9;
            
            // Mot à deviner
            string word = "";

            // Tableau contenant les étapes du dessin du pendu
            string[,] hangedMan = new string[NB_STEP, NB_LINE]
            {
                { "","","","","","","","","_____" },
                { "","  |","  |","  |","  |","  |","  |","  |","_____" },
                { "  --------","  |","  |","  |","  |","  |","  |","  |","_____" },
                { "  --------","  |       |","  |","  |","  |","  |","  |","  |","_____" },
                { "  --------","  |       |","  |       O","  |","  |","  |","  |","  |","_____" },
                { "  --------","  |       |","  |       O","  |      /|\\","  |","  |","  |","  |","_____" },
                { "  --------","  |       |","  |       O","  |      /|\\","  |       |","  |","  |","  |","_____" },
                { "  --------","  |       |","  |       O","  |      /|\\","  |       |","  |      / \\","  |","  |","_____" }
            };

            // Boucle de jeu permettant de recommencer
            do
            {
                // Affichage de l'introduction
                Console.Clear();
                Console.WriteLine(
                    "JEU DU PENDU\n" +
                    "============\n\n" +
                    "Bienvenue dans le jeu du pendu.\n" +
                    "L'objectif est de trouver un mot avant que le pendu ne soit entièrement dessiné.\n\n" +
                    "Veuillez donner un mot à trouver à l'abris du regard de celui qui doit le trouver !!\n" +
                    "(pas d'accents et uniquement des majuscules):"
                );

                // Récupération du mot à trouver
                word = Console.ReadLine();

                // Initialisation des variables nécessaires au fonctionnement
                bool isWordFound = false;
                bool isLetterFound = false;
                int nbLetterFound = 0;
                int attempt = 0;

                // Tableau contenant le mot incomplet
                // Initialement rempli de '_'
                char[] foundLetters = new char[word.Length];
                for (int i = 0; i < foundLetters.Length; ++i)
                {
                    foundLetters[i] = '_';
                }

                // Vider la console
                Console.Clear();

                // Nouvelle proposition de lettre
                while (attempt < NB_STEP - 1)
                {
                    // Préparer la tentative
                    isLetterFound = false;

                    // Prise de la lettre
                    Console.Write("\n\nVeuillez proposer une lettre : ");
                    char c = Convert.ToChar(Console.ReadLine());

                    // Vérification de la lettre
                    for (int i = 0; i < word.Length; ++i)
                    {
                        if (word[i] == c)
                        {
                            foundLetters[i] = c;
                            ++nbLetterFound;
                            isLetterFound = true;
                        }
                    }

                    // Bonne / mauvaise proposition
                    if (isLetterFound)
                    {
                        Console.WriteLine("Bonne proposition :-)");
                    }
                    else
                    {
                        Console.WriteLine("Mauvaise proposition :-(");
                        ++attempt;
                    }

                    // Affichage du pendu
                    for (int i = 0; i < NB_LINE; ++i)
                    {
                        Console.WriteLine(hangedMan[attempt, i]);
                    }

                    // Affichage du mot incomplet
                    Console.Write("\n   ");
                    for (int i = 0; i < foundLetters.Length; ++i)
                    {
                        Console.Write($"{foundLetters[i]} ");
                    }
                    Console.WriteLine("\n");

                    // Terminer la partie si le mot a été trouvé
                    if (nbLetterFound == word.Length)
                    {
                        Console.WriteLine(
                            $"BRAVO, le mot est trouvé: {word}\n" +
                            $"Vous avez gagné !!!");
                        isWordFound = true;
                        break;
                    }
                };

                // Si le mot n'a pas été trouvé
                if (!isWordFound)
                {
                    Console.WriteLine(
                        $"Le mot n'a pas été trouvé. Il s'agissait de : {word}\n" +
                        $"Vous avez perdu !!!");
                }

                // Demander à l'utilisateur s'il veut continuer à jouer.
                Console.WriteLine("Rejouer? [o]ui / [n]on");
            }
            while (Console.ReadKey(true).Key == ConsoleKey.O);
        } // Main()
    }
}
